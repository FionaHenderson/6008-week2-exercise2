﻿using Xamarin.Forms;

namespace _6008_week2_exercise2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();          
        }

        void Change_Something1(object sender, System.EventArgs e)
        {
            MainLabel.Text = "You Clicked the Button!";
            TheButton.Text = "I Have Been Clicked!";
            Background.BackgroundColor = Color.FromHex("#FF0");
            Background2.BackgroundColor = Color.FromHex("#FFF");
        }
    }
}
